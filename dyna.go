/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package dynago

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"net/http"
	"sync"
	"time"
)

// dynamicConfMgr maintains a map of config hashes to ids for dynamic
// inputs/outputs and thereby tracks whether a new configuration has changed for
// a particular id.
type dynamicConfMgr struct {
	configHashes map[string]string
}

func newDynamicConfMgr() *dynamicConfMgr {
	return &dynamicConfMgr{
		configHashes: map[string]string{},
	}
}

// Set will cache the config hash as the latest for the id and returns whether
// this hash is different to the previous config.
func (d *dynamicConfMgr) Set(id string, conf []byte) bool {
	hasher := sha256.New()
	hasher.Write(conf)
	newHash := hex.EncodeToString(hasher.Sum(nil))

	if hash, exists := d.configHashes[id]; exists {
		if hash == newHash {
			return false
		}
	}

	d.configHashes[id] = newHash
	return true
}

// Matches checks whether a provided config matches an existing config for the
// same id.
func (d *dynamicConfMgr) Matches(id string, conf []byte) bool {
	if hash, exists := d.configHashes[id]; exists {
		hasher := sha256.New()
		hasher.Write(conf)
		newHash := hex.EncodeToString(hasher.Sum(nil))

		if hash == newHash {
			return true
		}
	}

	return false
}

// Remove will delete a cached hash for id if there is one.
func (d *dynamicConfMgr) Remove(id string) {
	delete(d.configHashes, id)
}

// Dynamic is a type for exposing CRUD operations on dynamic broker
// configurations as an HTTP interface. Events can be registered for listening
// to configuration changes, and these events should be forwarded to the
// dynamic broker.
type Dynamic struct {
	onUpdate func(ctx context.Context, id string, conf []byte) error
	onDelete func(ctx context.Context, id string) error

	// configs is a map of the latest sanitised configs from our CRUD clients.
	configs      map[string][]byte
	configHashes *dynamicConfMgr
	configMut    sync.Mutex

	// ids is a map of dynamic components that are currently active and their
	// start times.
	ids    map[string]time.Time
	idsMut sync.Mutex
}

func NewDynamic() *Dynamic {
	return &Dynamic{
		onUpdate: func(ctx context.Context, id string, conf []byte) error {
			return nil
		},
		onDelete: func(ctx context.Context, id string) error {
			return nil
		},
		configs:      map[string][]byte{},
		configHashes: newDynamicConfMgr(),
		ids:          map[string]time.Time{},
	}
}

// OnUpdate registers a func to handle CRUD events where a request wants to set
// a new value for a dynamic configuration. An error should be returned if the
// configuration is invalid or the component failed.
func (d *Dynamic) OnUpdate(onUpdate func(ctx context.Context, id string, conf []byte) error) {
	d.onUpdate = onUpdate
}

// OnDelete registers a func to handle CRUD events where a request wants to
// remove a dynamic configuration. An error should be returned if the component
// failed to close.
func (d *Dynamic) OnDelete(onDelete func(ctx context.Context, id string) error) {
	d.onDelete = onDelete
}

// Stopped should be called whenever an active dynamic component has closed,
// whether by naturally winding down or from a request.
func (d *Dynamic) Stopped(id string) {
	d.idsMut.Lock()
	defer d.idsMut.Unlock()

	delete(d.ids, id)
}

// Started should be called whenever an active dynamic component has started
// with a new configuration. A normalised form of the configuration should be
// provided and will be delivered to clients that query the component contents.
func (d *Dynamic) Started(id string, config []byte) {
	d.idsMut.Lock()
	d.ids[id] = time.Now()
	d.idsMut.Unlock()

	if len(config) > 0 {
		d.configMut.Lock()
		d.configs[id] = config
		d.configMut.Unlock()
	}
}

func (d *Dynamic) List(w http.ResponseWriter, r *http.Request) {
	var httpErr error
	defer func() {
		if r.Body != nil {
			r.Body.Close()
		}
		if httpErr != nil {
			http.Error(w, "Internal server error", http.StatusBadGateway)
			return
		}
	}()

	type confInfo struct {
		Uptime string          `json:"uptime"`
		Config json.RawMessage `json:"config"`
	}

	uptimes := map[string]confInfo{}

	d.idsMut.Lock()
	for k, v := range d.ids {
		uptimes[k] = confInfo{
			Uptime: time.Since(v).String(),
			Config: []byte(`null`),
		}
	}
	d.idsMut.Lock()

	d.configMut.Lock()
	for k, v := range d.configs {
		if info, exists := uptimes[k]; exists {
			info.Config = v
			uptimes[k] = info
		} else {
			uptimes[k] = confInfo{
				Uptime: "stopped",
				Config: v,
			}
		}
	}
	d.configMut.Unlock()

	var resBytes []byte
	if resBytes, httpErr = json.Marshal(uptimes); httpErr == nil {
		w.Write(resBytes)
	}
}

func (d *Dynamic) Get(w http.ResponseWriter, r *http.Request) error {
	id := mux.Vars(r)["id"]

	d.configMut.Lock()
	conf, exists := d.configs[id]
	d.configMut.Unlock()

	if !exists {
		http.Error(w, fmt.Sprintf("Dynamic component '%v' is not active", id), http.StatusNotFound)
		return nil
	}

	w.Write(conf)
	return nil
}

func (d *Dynamic) Post(w http.ResponseWriter, r *http.Request) error {
	id := mux.Vars(r)["id"]

	reqBytes, err := io.ReadAll(r.Body)
	if err != nil {
		return err
	}

	d.configMut.Lock()
	matched := d.configHashes.Matches(id, reqBytes)
	d.configMut.Unlock()
	if matched {
		return nil
	}

	if err := d.onUpdate(r.Context(), id, reqBytes); err != nil {
		return err
	}

	d.configMut.Lock()
	d.configHashes.Set(id, reqBytes)
	d.configMut.Unlock()
	return nil
}

func (d *Dynamic) Delete(w http.ResponseWriter, r *http.Request) error {
	id := mux.Vars(r)["id"]

	if err := d.onDelete(r.Context(), id); err != nil {
		return err
	}

	d.configMut.Lock()
	d.configHashes.Remove(id)
	delete(d.configs, id)
	d.configMut.Unlock()

	return nil
}

func (d *Dynamic) CURD(w http.ResponseWriter, r *http.Request) {
	var httpErr error
	defer func() {
		if r.Body != nil {
			r.Body.Close()
		}
		if httpErr != nil {
			http.Error(w, fmt.Sprintf("Error: %v", httpErr), http.StatusBadGateway)
			return
		}
	}()

	id := mux.Vars(r)["id"]
	if id == "" {
		http.Error(w, "Var `id` must be set", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case "POST":
		httpErr = d.Post(w, r)
	case "GET":
		httpErr = d.Get(w, r)
	case "DELETE":
		httpErr = d.Delete(w, r)
	default:
		httpErr = fmt.Errorf("verb not supported: %v", r.Method)
	}
}
